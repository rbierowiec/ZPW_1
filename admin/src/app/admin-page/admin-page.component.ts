import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  title = 'Admin page';
  activePage = 'produkty';
  
  @Output() userStateChange = new EventEmitter<Boolean>();
  @Output() typeOfOrders;
  
  constructor(private cookieService: CookieService) { }

  ngOnInit() {
  }
  
  logout(){
    this.userStateChange.emit(false);
    this.cookieService.delete('token');
  }

}
