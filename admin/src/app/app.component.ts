import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  isUserLogged = false;

  constructor(private cookieService: CookieService){
    if(this.cookieService.get('token') != undefined && this.cookieService.get('token') != ""){
      this.isUserLogged = true;
    }
  };
}
