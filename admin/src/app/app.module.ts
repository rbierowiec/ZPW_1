import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgbModal, NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { ProductsComponent } from './products/products.component';
import { OrdersComponent } from './orders/orders.component';
import { productService } from './products/product.service';
import { ProductsNewItem } from './products/products.newitem.component';
import { CookieService } from 'ngx-cookie-service';
import { orderService } from './orders/orders.service';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { orderDetailsService } from './order-details/order-details.service';
import { categoryService } from './products/category.service';
import { PromotionWindowComponent } from './promotion-window/promotion-window.component';
import { promotionService } from './products/promotion.service';

@NgModule({
  declarations: [
    AppComponent,
    AdminPageComponent,
    LoginPageComponent,
    ProductsComponent,
    OrdersComponent,
    ProductsNewItem,
    OrderDetailsComponent,
    PromotionWindowComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  entryComponents: [
    ProductsNewItem,
    PromotionWindowComponent
  ],
  providers: [productService, ProductsNewItem, CookieService, orderService, orderDetailsService, categoryService, promotionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
