import { Component, Injectable, Output, EventEmitter, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})

@Injectable()
export class LoginPageComponent implements OnInit{

  @Output() userStateChange = new EventEmitter<Boolean>();

  constructor(private http: Http, private cookieService: CookieService) {
  }

  ngOnInit(){
  }

  authenticate(e){
    let username = e.target.elements[0].value;
    let password = e.target.elements[1].value;

    let tmpVariable = this;
    this.http.post('http://localhost:5500/users/login', {"username" : username, "password": password})
    .subscribe(
      (response) => {
        let result = response.json();
        this.cookieService.set('token', result.token);
        this.createUserSession();
      }
    );
  }

  createUserSession(){
    this.userStateChange.emit(true);
  }
}
