import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Order } from '../orders/order';
import { orderDetailsService } from './order-details.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  @Input() order: Order;

  @Output() orderStatusChange = new EventEmitter<Number>();

  orderDetails = {};
  productsName: Object = {};

  constructor(private orderDetailsService: orderDetailsService) {
  }

  ngOnInit() {
    this.orderDetailsService.getProductsNames(this.order.orderDetails, this);
  }
  
  getObjectKeys(object){
    return Object.keys(object);
  }

  markAsRealised(){
    this.orderStatusChange.emit(2);
  }
  
  calculateOrderSumup(){
    let totalPrice = 0;

    for ( let key in this.orderDetails ){
      totalPrice += this.orderDetails[key].count * this.orderDetails[key].price;
    }
    
    return totalPrice || 0;
  }
}
