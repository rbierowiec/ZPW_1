import {Injectable} from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class orderDetailsService { 

    constructor(private http:Http) {
    }
  
    getObservableInstance(){
        return this.http.get("http://localhost:5500/products", {withCredentials: true}).map((res: Response) => res.json());
    }
    
    getProductsNames(orderDetails, orderDetailsComponentInstance){
    let observableInstance = this.getObservableInstance();
    
    let orderDetailsObject = JSON.parse(orderDetails);
    let arrayOfProductsId = orderDetailsComponentInstance.getObjectKeys(orderDetailsObject)
    let arrayOfProductsName: Object = new Object;

    observableInstance.subscribe(
        (items) => {
            for (let i=0; i<items.length; i++){
                if ( arrayOfProductsId.includes(items[i]['_id']) ){
                    arrayOfProductsName[items[i]['_id']] = items[i]['name'];
                }
            }
            
            orderDetailsComponentInstance.productsName = arrayOfProductsName;
            orderDetailsComponentInstance.orderDetails = orderDetailsObject;
        },
        (error) => {
            console.log(error);
        }
    );
    }
}