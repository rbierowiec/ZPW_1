export class Order{    
    constructor(public id: number, public userId: number, public recipient: string, public address: string, public orderDetails: string, public status: Number, public date: Date){};
}