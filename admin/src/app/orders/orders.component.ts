import { Component, OnInit, Input } from '@angular/core';
import { orderService } from './orders.service';
import { Order } from './order';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  @Input() typeOfOrders;

  orders: Array<Order>;
  
  ngOnInit() {
  }

  constructor(private orderService: orderService) {
    this.orderService.getOrders(this);
  }

  getOrdersType(){
    if(this.typeOfOrders == 'oczekujace'){
      return "oczekujące";
    }else if(this.typeOfOrders == 'zrealizowane'){
      return "zrealizowane";
    }
  }

  updateOrderStatus(order, newValue){

    this.orders.forEach(function(item){
      if(item.id == order.id){
        item.status = newValue;
      }
    });
    
    this.orderService.updateOrderStatus(order, newValue);
  }

  getOrders(){
    var arrayOfOrders = Array<Order>();
    var thisAnchor = this;

    if(this.orders != undefined){
      this.orders.forEach(function(order){
        if(thisAnchor.typeOfOrders == 'oczekujace' && order.status == 1){
          arrayOfOrders.push(order);
        }else if(thisAnchor.typeOfOrders == 'zrealizowane' && order.status == 2){
          arrayOfOrders.push(order);
        }
      });
    }

    return arrayOfOrders;
  }

  calculateOrderSumup(orderIndex){
    // Wyciągnięcie listy zamówień konkretnego typu (oczekujące/zrealizowane)
    var filteredOrders = this.getOrders();
    let orderDescriptionObject = JSON.parse(filteredOrders[orderIndex].orderDetails);
    let totalPrice = 0;
    
    for ( let key in orderDescriptionObject ){
      totalPrice += orderDescriptionObject[key].count * orderDescriptionObject[key].price;
    }
    
    return totalPrice || 0;
  }
  
  prepareTableDescription(orderIndex){
    let orderDescriptionObject = JSON.parse(this.orders[orderIndex].orderDetails);
  }

}
