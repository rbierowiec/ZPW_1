import {Injectable} from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Product } from './product';
import { CookieService } from 'ngx-cookie-service';
import { Category } from './category';
import 'rxjs/add/operator/map';

@Injectable()
export class categoryService { 
  constructor(private http:Http, private cookieService: CookieService) {
  }

  getObservableInstance(){
    let headersVariable = new Headers({ 'x-access-token':  this.cookieService.get('token')});
    let options = new RequestOptions({ headers: headersVariable });

    return this.http.get("http://localhost:5500/categories", options).map((res: Response) => res.json());
  }

  getCategories(productComponentInstance){
    let observableInstance = this.getObservableInstance();

    observableInstance.subscribe(
        (items) => {
            let categories = new Array<Category>();
            
            for (let i=0; i<items.length; i++){
                categories.push(new Category(items[i]['_id'], items[i]['name']));
            }
            
            productComponentInstance.categories =  categories;
        },
        (error) => {
          console.log(error);
        }
    );
  }
}