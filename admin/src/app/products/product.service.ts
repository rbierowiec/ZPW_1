import {Injectable} from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Product } from './product';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';

@Injectable()
export class productService { 
  itemsInPage = 9999;

  constructor(private http:Http, private cookieService: CookieService) {
  }

  getObservableInstance(){
    let headersVariable = new Headers({ 'x-access-token':  this.cookieService.get('token')});
    let options = new RequestOptions({ headers: headersVariable });

    return this.http.get("http://localhost:5500/products", options).map((res: Response) => res.json());
  }

  getProducts(productComponentInstance){
    let observableInstance = this.getObservableInstance();

    observableInstance.subscribe(
        (items) => {
            let products = new Array<Product>();
            let index = 1;
            
            for (let i=0; i<items.length; i++){
                    products.push(new Product(items[i]['_id'], items[i]['name'], items[i]['description'], items[i]['photo'], items[i]['category'], items[i]['price']));
                  ++index;
            }
            
            productComponentInstance.products =  products;
        },
        (error) => {
          console.log(error);
        }
    );
  }

  removeProduct(productId){
    let headersVariable = new Headers({ 'x-access-token':  this.cookieService.get('token')});
    let options = new RequestOptions({ headers: headersVariable });

    let observableInstance = this.http.delete("http://localhost:5500/products/"+productId, options).map((res: Response) => res.json());
    observableInstance.subscribe();
  }

  addProduct(product, productComponentInstance){
    let headersVariable = new Headers({ 'Content-Type': 'application/json', 'x-access-token':  this.cookieService.get('token') });
    let options = new RequestOptions({ headers: headersVariable, withCredentials: true });

    return this.http.post("http://localhost:5500/products", JSON.stringify(product), options).toPromise()
    .then(function(){
      productComponentInstance.updateProductsList();
    })
    .catch();
  }

  updateProduct(product, productComponentInstance){
    let headersVariable = new Headers({ 'Content-Type': 'application/json', 'x-access-token':  this.cookieService.get('token')  });
    let options = new RequestOptions({ headers: headersVariable, withCredentials: true });

    return this.http.put("http://localhost:5500/products/"+product.id, JSON.stringify(product), options).toPromise()
    .then(function(){
      productComponentInstance.updateProductsList();
    })
    .catch();
  }
}