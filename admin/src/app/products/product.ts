export class Product{
    constructor(public id: number, public name: string, public description: string, public photo: String, public category: number, public price: number){};
}