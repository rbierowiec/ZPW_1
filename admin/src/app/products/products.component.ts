import { Component, OnInit } from '@angular/core';
import { productService } from './product.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ProductsNewItem } from './products.newitem.component';
import { PromotionWindowComponent } from '../promotion-window/promotion-window.component';
import { promotionService } from './promotion.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products = [];
  promotions = [];
  
  constructor(private productService: productService, private promotionService: promotionService, private modalService: NgbModal) { 
    this.promotionService.getPromotions(this);
    this.updateProductsList();
  }

  ngOnInit() {
  }

  updateProductsList(){
    this.productService.getProducts(this);
  }

  deleteItem(productKey){
    if (this.products[productKey] != undefined){
      this.productService.removeProduct(this.products[productKey].id);
      this.products.splice(productKey, 1);
    }
  }

  newItemDialog(){
    let modalRef = this.modalService.open(ProductsNewItem);
    modalRef.componentInstance.setAnchor(this);
    modalRef.componentInstance.title = "Nowy produkt";
  }

  editItem(productKey){
    let modalRef = this.modalService.open(ProductsNewItem);
    modalRef.componentInstance.setAnchor(this);
    modalRef.componentInstance.setProductValues(this.products[productKey].id, this.products[productKey].name, this.products[productKey].description, this.products[productKey].price, this.products[productKey].category);
  }

  createPromotion(productKey){
    let modalRef = this.modalService.open(PromotionWindowComponent);
    modalRef.componentInstance.promotion.productId = this.products[productKey].id;
    modalRef.componentInstance.setAnchor(this);
  }

  setPromotionAsActive(productId){
    this.promotions[productId] = true;
  }

  deletePromotion(productKey){
    this.promotionService.removePromotion(this.products[productKey].id);
    this.promotions[this.products[productKey].id] = false;
  }

}
