import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductsComponent } from './products.component';
import { productService } from './product.service';
import { categoryService } from './category.service';
import { Category } from './category';

@Component({
  selector: 'app-newitem-products',
  templateUrl: './products.newitem.component.html',
  styleUrls: ['./products.newitem.component.css']
})

export class ProductsNewItem {
    title = "Nowy produkt";
    product: Object = {};
    categories: Array<Category> = new Array<Category>();

    @ViewChild('fileInput') fileInput: ElementRef;

    productComponentInstance: ProductsComponent;

    constructor (private productService: productService, private categoryService: categoryService, public activeModal: NgbActiveModal){
        categoryService.getCategories(this);
    };

    // Ustawienie tzw. kotwicy, która pozwoli w późniejszym czasie odświeżyć listę produktów
    setAnchor(productComponentInstance){
        this.productComponentInstance = productComponentInstance;
    }

    setProductValues(id, name, description, price, category){
        this.product['id'] = id;
        this.product['name'] = name;
        this.product['description'] = description;
        this.product['price'] = price;
        this.product['category'] = category;
    }

    saveItem(productId){
        let reader = new FileReader();
        let fileBrowser = this.fileInput.nativeElement;

        if(fileBrowser.files && fileBrowser.files.length > 0){
            let file = fileBrowser.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.product['photo'] = reader.result;

                if (productId != undefined){
                    this.productService.updateProduct(this.product, this.productComponentInstance);
                }else{
                    this.productService.addProduct(this.product, this.productComponentInstance);
                }
            };
        }else{
            if (productId != undefined){
                this.productService.updateProduct(this.product, this.productComponentInstance);
            }else{
                this.productService.addProduct(this.product, this.productComponentInstance);
            }
        }
    }
}