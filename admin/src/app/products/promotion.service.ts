import {Injectable} from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Product } from './product';
import { CookieService } from 'ngx-cookie-service';
import { Category } from './category';
import 'rxjs/add/operator/map';

@Injectable()
export class promotionService { 
  constructor(private http:Http, private cookieService: CookieService) {
  }

  getObservableInstance(){
    let headersVariable = new Headers({ 'x-access-token':  this.cookieService.get('token')});
    let options = new RequestOptions({ headers: headersVariable });

    return this.http.get("http://localhost:5500/promotion", options).map((res: Response) => res.json());
  }

  getPromotions(productComponentInstance){
    let observableInstance = this.getObservableInstance();

    observableInstance.subscribe(
        (items) => {
            let promotions = new Object();

            for (var productId in items){
                promotions[productId] = true;
            }
            
            productComponentInstance.promotions =  promotions;
            console.log(promotions);
        },
        (error) => {
          console.log(error);
        }
    );
  }

  removePromotion(productId){
    let headersVariable = new Headers({ 'x-access-token':  this.cookieService.get('token')});
    let options = new RequestOptions({ headers: headersVariable });

    let observableInstance = this.http.delete("http://localhost:5500/promotion/"+productId, options).map((res: Response) => res.json());
    observableInstance.subscribe();
  }
}