import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionWindowComponent } from './promotion-window.component';

describe('PromotionWindowComponent', () => {
  let component: PromotionWindowComponent;
  let fixture: ComponentFixture<PromotionWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
