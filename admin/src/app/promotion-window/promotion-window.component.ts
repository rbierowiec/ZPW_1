import { Component, OnInit } from '@angular/core';
import { Promotion }  from './promotion';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Http, RequestOptions, Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { ProductsComponent } from '../products/products.component';

@Component({
  selector: 'app-promotion-window',
  templateUrl: './promotion-window.component.html',
  styleUrls: ['./promotion-window.component.css']
})
export class PromotionWindowComponent implements OnInit {

  title: String = "Dodaj promocję";
  promotion: Promotion = new Promotion('','',20,15);
  productComponentInstance: ProductsComponent;

  constructor(private http:Http, private cookieService: CookieService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  // Ustawienie tzw. kotwicy, która pozwoli w późniejszym czasie odświeżyć listę produktów
  setAnchor(productComponentInstance){
      this.productComponentInstance = productComponentInstance;
  }

  addPromotion(){
    let headersVariable = new Headers({ 'Content-Type': 'application/json', 'x-access-token':  this.cookieService.get('token') });
    let options = new RequestOptions({ headers: headersVariable, withCredentials: true });

    var thisAnchor = this;
    return this.http.post("http://localhost:5500/promotion", JSON.stringify(this.promotion), options).toPromise()
    .then(function(){
      thisAnchor.productComponentInstance.setPromotionAsActive(thisAnchor.promotion.productId);
    })
    .catch();
  }

}
