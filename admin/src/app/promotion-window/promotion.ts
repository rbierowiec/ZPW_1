export class Promotion{
    constructor(public id: string, public productId: string, public value: number, public time: number){};
}