import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { productService } from './products/product.service';
import { categoryService } from './categories/category.service';
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ShopPageComponent } from './shop-page/shop-page.component';
import { CategoriesComponent } from './categories/categories.component';
import { ProductsComponent } from './products/products.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { OrderFinalisingComponent } from './order-finalising/order-finalising.component';
import { ModalComponentComponent } from './modal-component/modal-component.component';
import { FilterSectionComponent } from './filter-section/filter-section.component';
import { NouisliderComponent, NouisliderModule } from 'ng2-nouislider';
import { LoginPageComponent } from './login-page/login-page.component';
import { CookieService } from 'ngx-cookie-service';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { myOrderService } from './my-orders/my-orders.service';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { orderDetailsService } from './order-details/order-details.service';
import { RegisterPageComponent } from './register-page/register-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ShopPageComponent,
    CategoriesComponent,
    ProductsComponent,
    ShoppingCartComponent,
    OrderFinalisingComponent,
    ModalComponentComponent,
    FilterSectionComponent,
    NouisliderComponent,
    LoginPageComponent,
    MyOrdersComponent,
    OrderDetailsComponent,
    RegisterPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot()
  ],
  entryComponents: [
    ModalComponentComponent
  ],
  providers: [productService, categoryService, CookieService, myOrderService, orderDetailsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
