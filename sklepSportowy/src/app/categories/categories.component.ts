import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { Category } from './category';
import { categoryService } from './category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  @Input() activeCategories: Array<boolean>;
  @Output() activeCategoriesChange = new EventEmitter<Array<boolean>>();

  categories = Array<Category>();
  
  constructor(private categoryService: categoryService) {
  }

  ngOnInit() {
    this.categoryService.getCategories(this);
  }

  // Emitter emitujący do rodzica informację o zmianie wartości
  setActive(id){
    this.activeCategories[id] = !this.activeCategories[id];
    this.activeCategoriesChange.emit(this.activeCategories);
  }
}
