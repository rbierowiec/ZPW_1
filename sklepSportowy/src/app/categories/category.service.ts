import {Injectable} from '@angular/core';
import { Category } from './category';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class categoryService { 
  constructor(private http:Http) {}
  
  getObservableInstance(){
    return this.http.get("http://localhost:5500/categories").map((res: Response) => res.json());
  }

  getCategories(categoryComponentInstance) {
    let observableInstance = this.getObservableInstance();

    observableInstance.subscribe(
        (items) => {
            let categories = new Array<Category>();
            let activeCategories = {};
            
            for (let i=0; i<items.length; i++){
              categories.push(new Category(items[i]['_id'], items[i]['name']));
              activeCategories[items[i]['_id']] = false;
            }
            
            categoryComponentInstance.categories = categories;
            categoryComponentInstance.activeCategories = activeCategories;

            if(items.length > 0){
              categoryComponentInstance.setActive(items[0]['_id']);
            }
        },
        (error) => {
          console.log(error);
        }
    );
  }
}