import { Component, OnInit, Output, EventEmitter, Input, Injectable, DoCheck } from '@angular/core';
import { NouisliderModule } from 'ng2-nouislider';

@Component({
  selector: 'app-filter-section',
  templateUrl: './filter-section.component.html',
  styleUrls: ['./filter-section.component.css']
})

@Injectable()
export class FilterSectionComponent implements DoCheck {

  @Input() filterName: String;
  @Input() filterPrice: Array<Number>;
  @Input() filterPriceRange: Array<Number>;

  @Output() filterNameChange = new EventEmitter<String>();
  @Output() filterPriceChange = new EventEmitter<Array<Number>>();

  constructor() { }

  ngDoCheck() {
    // if(this.filterPrice[0] == -1 && this.filterPrice[1] == -1 && this.filterPriceRange != undefined && this.filterPriceRange[1] != 0){
    //   this.filterPrice[0] = this.filterPriceRange[0];
    //   this.filterPrice[1] = this.filterPriceRange[1];
    // }
  }

  updateFilterName(value){
    this.filterNameChange.emit(value);
  }

  updateFilterPrice(){
    this.filterPriceChange.emit(this.filterPrice);
  }

  getFilterRange(key){
    return (this.filterPriceRange != undefined && this.filterPriceRange[key] != undefined) ? this.filterPriceRange[key] : key;
  }

}
