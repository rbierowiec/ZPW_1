import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title = "Sklep sportowy";
  
  @Input() itemsInCart;
  @Input() shoppingCartValue;
  @Input() pageActive;
  @Input() userLogged;

  @Output() shoppingCartActiveChange = new EventEmitter<String>();
  @Output() logoutUserEvent = new EventEmitter<Boolean>();

  constructor(private cookieService: CookieService) { }

  ngOnInit() {
  }

  goToPage(pageName){
    this.shoppingCartActiveChange.emit(pageName);
  }

  logoutUser(){
    this.cookieService.delete('token');
    this.logoutUserEvent.emit(false);
  }

}
