import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import { ModalComponentComponent } from '../modal-component/modal-component.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  
  @Output() userStateChange = new EventEmitter<Boolean>();
  @Output() goToRegistrationPageEvent = new EventEmitter<Boolean>();

    constructor(private http: Http, private cookieService: CookieService, private modalService: NgbModal) {
    }
  
    ngOnInit(){
    }
  
    authenticate(e){
      let username = e.target.elements[0].value;
      let password = e.target.elements[1].value;
  
      let tmpVariable = this;
      this.http.post('http://localhost:5500/users/login', {"username" : username, "password": password}).toPromise()
      .then(
        (response) => {
          let result = response.json();
          this.cookieService.set('token', result.token);
          this.createUserSession();
        }
      )
      .catch(function(){
        tmpVariable.openDialog("Błąd", "Błędne dane logowania", "bg-danger");
      });
    }
    
    openDialog(title, msg, type){
      let modalRef = this.modalService.open(ModalComponentComponent);
      
      modalRef.componentInstance.title = title;
      modalRef.componentInstance.body = msg;
      modalRef.componentInstance.modal_type = type;
    }

    goToRegistrationPage(){
      this.goToRegistrationPageEvent.emit(true);
    }
  
    createUserSession(){
      this.userStateChange.emit(true);
    }
}
