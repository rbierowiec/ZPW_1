import { Component, Input, Injectable } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-component',
  templateUrl: './modal-component.component.html',
  styleUrls: ['./modal-component.component.css']
})

@Injectable()
export class ModalComponentComponent {
  title = "test";
  body = "test";
  modal_type = "bg-success";
  
  constructor(public activeModal: NgbActiveModal) {}
}
