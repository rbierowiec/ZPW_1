import { Component, OnInit } from '@angular/core';
import { Order } from '../order-finalising/order';
import { myOrderService } from './my-orders.service';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit {
  
  orders: Array<Order>;


  constructor(private orderService: myOrderService) {
    this.orderService.getOrders(this);
  }

  ngOnInit() {
  }
  
  calculateOrderSumup(orderIndex){
    // Wyciągnięcie listy zamówień konkretnego typu (oczekujące/zrealizowane)
    let orderDescriptionObject = JSON.parse(this.orders[orderIndex].orderDetails);
    let totalPrice = 0;
    
    for ( let key in orderDescriptionObject ){
      totalPrice += orderDescriptionObject[key].count * orderDescriptionObject[key].price;
    }
    
    return totalPrice || 0;
  }
  
  prepareTableDescription(orderIndex){
    let orderDescriptionObject = JSON.parse(this.orders[orderIndex].orderDetails);
  }

}
