import {Injectable} from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Order } from '../order-finalising/order';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';

@Injectable()
export class myOrderService { 

    constructor(private http:Http, private cookieService: CookieService) {
    }
  
    getObservableInstance(){
        let headersVariable = new Headers({ 'x-access-token':  this.cookieService.get('token')});
        let options = new RequestOptions({ headers: headersVariable });

        return this.http.get("http://localhost:5500/orders/me", options).map((res: Response) => res.json());
    }
    
    getOrders(productComponentInstance){
        let observableInstance = this.getObservableInstance();

        observableInstance.subscribe(
            (items) => {
                let orders = new Array<Order>();
                let index = 1;
                
                for (let i=0; i<items.length; i++){
                    orders.push(new Order(items[i]['_id'], items[i]['userId'], items[i]['recipient'], items[i]['address'], items[i]['orderDetails'], items[i]['status'], items[i]['date']));
                    ++index;
                }
                
                console.log(orders);
                productComponentInstance.orders =  orders;
            },
            (error) => {
                console.log(error);
            }
        );
    }
}