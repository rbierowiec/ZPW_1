import { Component, OnInit, Input,  } from '@angular/core';
import { Order } from '../order-finalising/order';
import { orderDetailsService } from './order-details.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
  
    @Input() order: Order;
  
    orderDetails = {};
    productsName: Object = {};
  
    constructor(private orderDetailsService: orderDetailsService) {
    }
  
    ngOnInit() {
      this.orderDetailsService.getProductsNames(this.order.orderDetails, this);
    }
    
    getObjectKeys(object){
      return Object.keys(object);
    }
    
    calculateOrderSumup(){
      let totalPrice = 0;
  
      for ( let key in this.orderDetails ){
        totalPrice += this.orderDetails[key].count * this.orderDetails[key].price;
      }
      
      return totalPrice || 0;
    }
}
