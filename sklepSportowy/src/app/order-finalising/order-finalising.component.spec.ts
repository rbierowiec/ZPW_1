import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderFinalisingComponent } from './order-finalising.component';

describe('OrderFinalisingComponent', () => {
  let component: OrderFinalisingComponent;
  let fixture: ComponentFixture<OrderFinalisingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderFinalisingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFinalisingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
