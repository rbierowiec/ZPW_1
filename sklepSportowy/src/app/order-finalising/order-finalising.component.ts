import { Component, OnInit, Input, Injectable, Output, EventEmitter } from '@angular/core';
import { Order } from './order';
import { FormControl } from '@angular/forms';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ModalComponentComponent } from '../modal-component/modal-component.component';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-order-finalising',
  templateUrl: './order-finalising.component.html',
  styleUrls: ['./order-finalising.component.css']
})

@Injectable()
export class OrderFinalisingComponent implements OnInit {

  @Input() arrayOfItemsInCart: Object;
  @Input() promotionsArray: Object = new Object();

  @Output() cleanTheShoppingCart: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  myModel: Order = new Order("","1","","","", 1, new Date(Date.now()));

  constructor(private http:Http, private modalService: NgbModal, private cookieService: CookieService) { 
  }

  ngOnInit() {
  }

  compressOrderObject(arrayOfItemsInCart){
    let resultObject = {};
    
    for (let key in arrayOfItemsInCart){
      resultObject[key] = {};
      resultObject[key]['count'] = arrayOfItemsInCart[key].count;
      resultObject[key]['price'] = arrayOfItemsInCart[key]['productDescription'].price;

      // Jeżeli wystepuje promocja na produkt - zastosuj ją
      if(this.promotionsArray[key] != undefined){
        resultObject[key]['price'] *= ((100-this.promotionsArray[key]['promotionValue']) / 100);
      }
    }

    return resultObject;
  }

  sendOrder(){
    this.myModel.orderDetails = JSON.stringify(this.compressOrderObject(this.arrayOfItemsInCart));

    let headersVariable = new Headers({ 'x-access-token':  this.cookieService.get('token'), 'Content-Type': 'application/json' });
    let options = new RequestOptions({headers: headersVariable });
    
    let tmpVariable = this;
    return this.http.post("http://localhost:5500/orders", this.myModel, options).toPromise()
    .then(function(){
      tmpVariable.openDialog();
    })
    .catch();
  }

  openDialog(){
    let modalRef = this.modalService.open(ModalComponentComponent);
    
    modalRef.componentInstance.title = 'Informacja';
    modalRef.componentInstance.body = 'Zamówienie wysłano pomyślnie';

    this.cleanTheShoppingCart.emit(true);
  }
}