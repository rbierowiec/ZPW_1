export class Order{    
    constructor(public id: String, public userId: String, public recipient: string, public address: string, public orderDetails: string, public status: Number, public date: Date){};
}