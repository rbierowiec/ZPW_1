import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import { Product } from './product';
import 'rxjs/add/operator/map';

@Injectable()
export class productService { 
  itemsInPage = 3;

  constructor(private http:Http) {
  }

  getObservableInstance(){
    return this.http.get("http://localhost:5500/products").map((res: Response) => res.json());
  }

  getProducts(activeCategories, pageNumber = 1, productComponentInstance){
    let observableInstance = this.getObservableInstance();

    observableInstance.subscribe(
        (items) => {
            let products = new Array<Product>();
            let index = 1;
            
            for (let i=0; i<items.length; i++){
                if(activeCategories[items[i]['category']]){
                    products.push(new Product(items[i]['_id'], items[i]['name'], items[i]['description'], items[i]['category'], items[i]['price'], items[i]['photo']));
                  ++index;
                }
            }

            // Przekazanie produktów do komponentu - tam odbędzie się wyliczenie ilości stron oraz filtracja produktów
            productComponentInstance.products =  products;
        },
        (error) => {
          console.log(error);
        }
    );
  }
}