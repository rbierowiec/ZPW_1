export class Product{
    constructor(public id: number, public name: string, public description: string, public category: number, public price: number, public photo: string){};
}