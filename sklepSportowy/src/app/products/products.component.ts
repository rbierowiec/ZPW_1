import { Injectable, Component, OnInit, Input, SimpleChanges, Output, EventEmitter, DoCheck, IterableDiffers } from '@angular/core';
import { Product } from './product';
import { productService } from './product.service';
import { ModalComponentComponent } from '../modal-component/modal-component.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

@Injectable()
export class ProductsComponent implements DoCheck {

  // Aktualnie aktywna kategoria
  @Input() activeCategories: Object;
  @Input() arrayOfItemsInCart: Object;
  @Input() promotionsArray: Object;
  
  @Input() filterName: String;
  @Input() filterPrice: Array<Number>;

  @Output() filterPriceRangeChange = new EventEmitter<Array<Number>>();
  @Output() arrayOfItemsInCartChange = new EventEmitter<Object>();

  products: Array<Product>;
  activeCategoriesClone: Object = new Object();

  filterNameClone: String = "";
  filterPriceClone: Array<Number> = [-1,-1];

  // Aktualnie aktywna strona oraz ilość stron dla danej kategorii
  activePage = 1;

  pagesNumber = 0;
  maxItemsInPage = 3;

  constructor(private productService: productService, private modalService: NgbModal) {
  }

  // Funkcja tworząca tablicę do paginacji
  createRange(index){
    var array = new Array(index);

    for(let i=1; i<= index; i++){
      array[i-1] = i;
    }
    
    return array;
  }

  createCountDown(promotionProduct){
    var secondsLeft = promotionProduct['timeToTheEnd'];

    var h = Math.floor(secondsLeft / 3600);
    var m = Math.floor(secondsLeft % 3600 / 60);
    var s = Math.floor(secondsLeft % 3600 % 60);
    
    var timeString = ("0"+h).slice(-2) + ":" + ("0"+m).slice(-2) + ":" + ("0"+s).slice(-2);

    return timeString; 
  }

  checkActiveCategoriesChange(){
    for (let key in this.activeCategories){
      if(this.activeCategoriesClone[key] != this.activeCategories[key]){
        return true;
      }
    }

    return false;
  }

  checkFilterValueChange(){
    var changed = false;
    if(this.filterName != undefined && this.filterPrice != undefined){
      if(this.filterName != this.filterNameClone || this.filterPriceClone[0] != this.filterPrice[0] || this.filterPriceClone[1] != this.filterPrice[1]){
        changed = true;
  
        this.filterNameClone = this.filterName;
        this.filterPriceClone = this.filterPrice;
      }
    }

    return changed;
  }

  ngDoCheck() {
    let changes = this.checkActiveCategoriesChange();
    
    // Wykonaj aktualizację tylko wtedy gdy zmieniła się tablica activeCategories
    if(changes){
      this.updateActiveProducts();

      // Stwórz kopię tablicy activeCategories, nie przypisuj jej referencji
      this.activeCategoriesClone = Object.assign({}, this.activeCategories);
    }else if(this.checkFilterValueChange()){
      this.updateActiveProducts(false);
    }
  }

  filterProducts(){
    var itemsAfterFiltered = new Array<Product>();

    var thisAnchor = this;

    if(this.products != undefined){
      var itemIndex = 0;
      var rangeValue = [-1, -1];

      this.products.forEach(function(item){
        var validation = true;
        if(thisAnchor.filterName != "" && item.name.toLowerCase().indexOf(thisAnchor.filterName.toString().toLowerCase()) == -1){
          validation = false;
        }
        if((thisAnchor.filterPrice[0] > -1 && thisAnchor.filterPrice[0] > item.price) || (thisAnchor.filterPrice[1] > -1 && thisAnchor.filterPrice[1] < item.price)){
          validation = false;
        }
  
        if(validation){
            if(itemIndex+1 <= thisAnchor.activePage * thisAnchor.maxItemsInPage && itemIndex+1 > (thisAnchor.activePage - 1) * thisAnchor.maxItemsInPage){
              itemsAfterFiltered.push(item);
            }
            ++itemIndex;
        }
        
        if(item.price < rangeValue[0] || rangeValue[0] == -1){
          rangeValue[0] = item.price;
        }
        if(item.price > rangeValue[1] || rangeValue[1] == -1){
          rangeValue[1] = item.price;
        }
      })
  
      // Wyliczenie ilości stron z produktami
      thisAnchor.pagesNumber = Math.ceil(itemIndex / thisAnchor.maxItemsInPage);
      if(thisAnchor.activePage > thisAnchor.pagesNumber){
        thisAnchor.activePage = thisAnchor.pagesNumber;
      }else if(thisAnchor.activePage == 0){
        thisAnchor.activePage = 1;
      }
      
      thisAnchor.filterPriceRangeChange.emit(rangeValue);
    }

    return itemsAfterFiltered;
  }

  updateActiveProducts(reloadProducts = true){
    this.productService.getProducts(this.activeCategories, this.activePage, this);
  }

  updateActivePage(pageNumber){
    if(pageNumber != this.activePage && pageNumber >0 && pageNumber <= this.pagesNumber){
      this.activePage = pageNumber;
      this.updateActiveProducts();
    }
  }

  addItemToCart(item){
    if(this.arrayOfItemsInCart[item.id] != undefined){
      this.arrayOfItemsInCart[item.id]['count'] += 1;
    }else{
      this.arrayOfItemsInCart[item.id] = {};
      this.arrayOfItemsInCart[item.id]['count'] = 1;
      this.arrayOfItemsInCart[item.id]['productDescription'] = item;
    }
    
    this.updateShoppingCart();
  }

  // Emitter emitujący do rodzica informację o zmianie wartości
  updateShoppingCart(){
    this.arrayOfItemsInCartChange.emit(this.arrayOfItemsInCart);
  }

  showProduct(product){
    var description = "";

    if(product.photo != undefined && product.photo != ""){
      description = "<img width='400px' align='center' src='"+product.photo+"' /><br/>";
    }
    description += product.description;
    this.openDialog(product.name, description, "bg-success");
  }
  
  openDialog(title, msg, type){
    let modalRef = this.modalService.open(ModalComponentComponent);
    
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.body = msg;
    modalRef.componentInstance.modal_type = type;
  }

}
