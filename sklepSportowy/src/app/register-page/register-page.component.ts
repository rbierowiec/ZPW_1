import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import { ModalComponentComponent } from '../modal-component/modal-component.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  @Output() goToLoginPage = new EventEmitter<Boolean>();

  constructor(private http: Http, private modalService: NgbModal) { }

  ngOnInit() {
  }
  
  registerUser(e){
    let username = e.target.elements[0].value;
    let password = e.target.elements[1].value;

    let tmpVariable = this;
    this.http.post('http://localhost:5500/users/register', {"username" : username, "password": password}).toPromise()
    .then(function(e){
      tmpVariable.goToLoginPage.emit(true);
    })
    .catch(function(e){
      tmpVariable.openDialog("Błąd","Istnieje już użytkownik o podanym loginie","bg-danger");
    });
  }

  openDialog(title, msg, type){
    let modalRef = this.modalService.open(ModalComponentComponent);
    
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.body = msg;
    modalRef.componentInstance.modal_type = type;
  }

}
