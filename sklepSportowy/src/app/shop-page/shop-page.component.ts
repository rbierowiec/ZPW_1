import { Component, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import io from 'socket.io-client';

@Component({
  selector: 'app-shop-page',
  templateUrl: './shop-page.component.html',
  styleUrls: ['./shop-page.component.css']
})

export class ShopPageComponent implements OnInit {

  @Output() activeCategories: Object = new Object();
  @Output() shoppingCartValue: number = 0;
  @Output() arrayOfItemsInCart = {};
  @Output() itemsInCart = 0;
  @Output() pageActive = "shopping";

  @Output() filterName: String = "";
  @Output() filterPrice;
  @Output() filterPriceRange;
  @Output() userLogged;

  @Output() promotionsArray: Object = new Object();

  constructor(private cookieService: CookieService) {
    this.filterPrice = [0, 3000];
    this.filterPriceRange = [-1, 0];

    if(this.cookieService.get('token') != undefined && this.cookieService.get('token') != ""){
      this.userLogged = true;
    }
  }

  ngOnInit() {
    var socket = io.connect('http://localhost:5500');
    var thisAnchor = this;

    socket.on('promotion' , function (data) {
      if(thisAnchor.promotionsArray[data.productId] == undefined){
        thisAnchor.promotionsArray[data.productId] = new Object();
        
        thisAnchor.promotionsArray[data.productId]['promotionValue'] = data.promotionValue;
        thisAnchor.promotionsArray[data.productId]['timeToTheEnd'] = data.timeToTheEnd;
        thisAnchor.promotionsArray[data.productId]['loadAt'] = (new Date()).getTime();

        var intervalInstance = setInterval(function(){
          if(thisAnchor.promotionsArray[data.productId] == undefined || thisAnchor.promotionsArray[data.productId]['timeToTheEnd'] <= 0){
            clearInterval(intervalInstance);
            thisAnchor.calculateItemsInCart();
          }else{
            --thisAnchor.promotionsArray[data.productId]['timeToTheEnd'];
          }
        }, 1000);
      }
    });

    socket.on('promotions' , function (data) {
      if(thisAnchor.promotionsArray[data.productId] == undefined){
        thisAnchor.promotionsArray[data.productId] = new Object();
      }

      for(var key in data){
        var promotion = data[key];

        if(thisAnchor.promotionsArray[key] == undefined){
          thisAnchor.promotionsArray[key] = new Object();
        }

        thisAnchor.promotionsArray[key]['promotionValue'] = promotion.promotionValue;
        thisAnchor.promotionsArray[key]['loadAt'] = promotion.startDate;
        thisAnchor.promotionsArray[key]['timeToTheEnd'] = promotion.promotionTime - (Math.floor(((new Date()).getTime() - promotion.startDate)/1000));

        var intervalInstance = setInterval(function(){
          if(thisAnchor.promotionsArray[key] == undefined || thisAnchor.promotionsArray[key]['timeToTheEnd'] <= 0){
            clearInterval(intervalInstance);
            thisAnchor.calculateItemsInCart();
          }else{
            --thisAnchor.promotionsArray[key]['timeToTheEnd'];
          }
        }, 1000);
      };
    });

    socket.on('productDeleted' , function (data) {
      // Skrypt powodujący przeładowanie produktów w koszyku
      var tmp = thisAnchor.filterName;
      thisAnchor.filterName = "changedFilterName";
      setTimeout(function(){
        thisAnchor.filterName = tmp;
      },1);
    });

    socket.on('promotionEnd' , function (data) {
      if(thisAnchor.promotionsArray[data.productId] != undefined){
        delete(thisAnchor.promotionsArray[data.productId]);
      }
    });
  }

  // Funkcja uruchamiana w momencie aktualizacji koszyka, zlicza ilość elementów w koszyku oraz aktualizuje jego wartość
  calculateItemsInCart(){
    let itemsInCart = 0;
    let shoppingCartValue = 0;

    Object.keys(this.arrayOfItemsInCart).map((key) =>{
      var singleItemPrice = this.arrayOfItemsInCart[key]['productDescription'].price;

      // Jeżeli wystepuje promocja na produkt - zastosuj ją
      if(this.promotionsArray[key] != undefined){
        singleItemPrice *= ((100-this.promotionsArray[key]['promotionValue']) / 100);
      }

      itemsInCart += this.arrayOfItemsInCart[key]['count'];
      shoppingCartValue += this.arrayOfItemsInCart[key]['count'] * singleItemPrice;
    });

    this.shoppingCartValue = shoppingCartValue;
    this.itemsInCart = itemsInCart;
  }

  cleanTheShoppingCart(){
    this.shoppingCartValue = 0;
    this.itemsInCart = 0;
    this.arrayOfItemsInCart = {};
    this.pageActive = "shopping";
  }

}
