import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  @Input() arrayOfItemsInCart: Object;
  @Input() promotionsArray: Object = new Object();
  @Input() shoppingCartValue;

  @Output() shoppingCartActiveChange = new EventEmitter<String>();
  @Output() arrayOfItemsInCartChange = new EventEmitter<Object>();

  constructor() { }

  ngOnInit() {
  }

  getObjectKeys(object){
    return Object.keys(object);
  }

  getProductPrice(key){
    var singleItemPrice = this.arrayOfItemsInCart[key].productDescription.price;

    // Jeżeli wystepuje promocja na produkt - zastosuj ją
    if(this.promotionsArray[key] != undefined){
      singleItemPrice *= ((100-this.promotionsArray[key]['promotionValue']) / 100);
    }

    return singleItemPrice;

  }
  
  continueShopping(){
    this.shoppingCartActiveChange.emit("shopping");
  }
  
  finaliseOrder(){
    if(this.shoppingCartValue > 0){
      this.shoppingCartActiveChange.emit("orderFinalising");
    }
  }

  removeFromCart(productId){
    delete(this.arrayOfItemsInCart[productId]);
    this.arrayOfItemsInCartChange.emit(this.arrayOfItemsInCart);
  }

}
