'use strict';

var mongoose = require('mongoose'),
Category = mongoose.model('Category');

exports.getAllCategories = function(app, req, res) {
    Category.find({}, function(err, task) {
        if (err){
            res.send(err);
        }
        res.json(task);
    });
};

exports.createNewCategory = function(app, req, res) {
    var newCategory = new Category(req.body);
    newCategory.save(function(err, task) {
        if (err){
            res.send(err);
        }
        res.json(task);
    });
};

exports.deleteCategory = function(app, req, res) {
    Category.remove({
        _id: req.params.id
    }, function(err, task) {
        if (err){
            res.send(err);
        }
        res.json({ message: 'Task successfully deleted' });
    });
};

exports.updateCategory = function(app, req, res) {
    Category.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, task) {
        if (err){
            res.send(err);
        }
        res.json(task);
  });
};