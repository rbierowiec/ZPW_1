'use strict';

var mongoose = require('mongoose'),
Order = mongoose.model('Order');

exports.getAllOrders = function(app, req, res) {
    Order.find({}, function(err, task) {
        if (err){
            res.send(err);
        }
        res.json(task);
    });
};

exports.getAllUserOrders = function(app, req, res) {
    Order.find({'userId': req.decoded.id}, function(err, task) {
        if (err){
            res.send(err);
        }
        res.json(task);
    });
};

exports.createNewOrder = function(app, req, res) {
    if(req.body.id != undefined){
        delete req.body.id;
    }
    var newOrder = new Order(req.body);
    if(req.decoded != undefined){
        newOrder.userId = req.decoded.id;
    }
    newOrder.save(function(err, task) {
        if (err){
            res.send(err);
        }
        res.json(task);
    });
};

exports.deleteOrder = function(app, req, res) {
    Order.remove({
        _id: req.params.id
    }, function(err, task) {
        if (err){
            res.send(err);
        }
        res.json({ message: 'Task successfully deleted' });
    });
};

exports.updateOrder = function(app, req, res) {
    Order.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, task) {
        if (err){
            res.send(err);
        }
        console.log(req.body);
        res.json(task);
  });
};