'use strict';

var mongoose = require('mongoose'),
Product = mongoose.model('Product');

exports.getAllProducts = function(app, req, res) {
    Product.find({}, function(err, task) {
        if (err){
            res.send(err);
        }
        res.json(task);
    });
};

exports.createNewProduct = function(app, req, res) {
    var newProduct = new Product(req.body);
    newProduct.save(function(err, task) {
        if (err){
            res.send(err);
        }
        res.json(task);
    });
};

exports.deleteProduct = function(app, req, res) {
    Product.remove({
        _id: req.params.id
    }, function(err, task) {
        if (err){
            res.send(err);
        }else{
            var io = app.get('io_socket');
            io.emit('productDeleted' , { productId: req.params.id });
            res.json({ message: 'Task successfully deleted' });
        }
    });
};

exports.updateProduct = function(app, req, res) {
    Product.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, task) {
        if (err){
            res.send(err);
        }
        res.json(task);
  });
};