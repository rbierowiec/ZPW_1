'use strict';

var mongoose = require('mongoose');
var Product = mongoose.model('Product');
var promotionsArray = {};
var timeoutsId = {};

exports.createSocketInstance = function(app){
    var io = app.get('io_socket');
    
    io.on('connection' , function(client) {
        client.emit('promotions' , promotionsArray);
    });
}

exports.getAllPromotions = function(app, req, res) {
    res.json(promotionsArray);
};

exports.createNewPromotion = function(app, req, res) {
    var io = app.get('io_socket');
    io.emit('promotion' , { productId: req.body.productId, promotionValue: req.body.value, timeToTheEnd: req.body.time*60 });

    promotionsArray[req.body.productId] = {};
    promotionsArray[req.body.productId]['promotionValue'] = req.body.value;
    promotionsArray[req.body.productId]['promotionTime'] =  req.body.time*60;
    promotionsArray[req.body.productId]['startDate'] =  new Date().getTime();

    var setTimeoutId = setTimeout(function(){
        io.emit('promotionEnd' , { productId: req.body.productId });
        clearTimeout(this);
        delete promotionsArray[req.body.productId];
        delete timeoutsId[req.body.productId];
    },promotionsArray[req.body.productId]['promotionTime'] * 1000);

    timeoutsId[req.body.productId] =  setTimeoutId;
    res.status(200);
    res.json(true);
};

exports.deletePromotion = function(app, req, res){
    var io = app.get('io_socket');
    if (promotionsArray[req.params.id] != undefined){
        io.emit('promotionEnd' , { productId: req.params.id });

        clearTimeout(timeoutsId[req.params.id]);
        delete promotionsArray[req.params.id];
        delete timeoutsId[req.params.id];
        res.status(200);
        res.json(true);
    }
}

function randPromotionValue(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Funkcja pierwotnie służyła do generowania losowych promocji
exports.generatePromotion = function(io, maxPromotionValue, promotionTime){
    var productsObject = new Array();
    
	Product.find({},
        function(err, task){
            task.forEach(function(item){
                productsObject.push(item['_id']);
            });

            var startDate = new Date();

            var randomProduct = productsObject[Math.floor(Math.random() * productsObject.length)];
            var promotionValue = randPromotionValue(0, maxPromotionValue);
            var promotionTimeSeconds = Math.floor(promotionTime * 60);

            io.emit('promotions' , { productId: randomProduct, promotionValue: promotionValue, timeToTheEnd: promotionTimeSeconds });

            io.sockets.removeAllListeners();
            io.on('connection' , function(client) {
                var secondsFromTheStart = Math.floor((new Date().getTime() - startDate.getTime()) / 1000);
                client.emit('promotions' , { productId: randomProduct, promotionValue: promotionValue, timeToTheEnd: (promotionTimeSeconds - secondsFromTheStart) });
            });

            setTimeout(function(){
                io.emit('promotionEnd' , { productId: randomProduct });
                clearTimeout(this);
            }, promotionTime * 60 * 1000)
        }
    );
};