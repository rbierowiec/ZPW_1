'use strict';

var jwt    	        = require('jsonwebtoken'),
config 		    = require('../config.js');

exports.checkToken = function(app, req, res, next, tokenRequired = true){
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {
        jwt.verify(token, config.secret, function(err, decoded) { 
            if (err) {
                res.status(401);
                res.json({ success: false, message: 'Failed to authenticate token.' });    
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded; 
                next(app, req, res);
            }
        });
    } else {
        if(tokenRequired){
            res.status(403).send({ 
                success: false, 
                message: 'No token provided.' 
            });
        }else{
            next(app, req, res);
        }
        
    }
}