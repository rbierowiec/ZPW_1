'use strict';

var mongoose    = require('mongoose'),
User            = mongoose.model('User'),
passwordHash    = require('password-hash'),
jwt    	        = require('jsonwebtoken'),
config 		    = require('../config.js');


exports.loginUser = function(app, req, res) {
    User.findOne({ username: req.body.username }, function(err, user) {
        if (err){
            throw err;
        }

        if(user == null || !passwordHash.verify(req.body.password, user.password)){
            res.status(401);
            res.json({ message: 'Invalid username or password' });
        }else{
            
            const payload = {
                admin: true, // in future can change for user.admin
                id: user._id
            };

            var token = jwt.sign(payload, config.secret, {
                expiresIn : 60*60*24 // expires in 24 hours
            });

            res.json({ message: 'Logged successfully', token: token });
        }
    });
};

exports.registerUser = function(app, req, res) {
    var newUser = new User(req.body);
    newUser.password = passwordHash.generate(newUser.password);

    newUser.save(function(err, task) {
        if (err){
            res.status(400);
            res.send(err);
        }
        res.json(task);
    });
};