function createModels(Schema, mongoose){
	
    var ProductSchema = mongoose.Schema({
      id : { type: Schema.Types.ObjectId },
			name: String,
			address: String,
			description: String,
			photo: String,
			price: Number,
			category: String
    });
	
    var OrderSchema = mongoose.Schema({
			id : { type: Schema.Types.ObjectId },
			userId: String,
			recipient: String,
			address: String,
			orderDetails: String,
			status: Number,
			date: Date
    });
	
    var CategorySchema = mongoose.Schema({
      id : { type: Schema.Types.ObjectId },
	  name: String
	});
	
	var UserSchema = new Schema({
		username: { type: String, required: true, index: { unique: true } },
		password: { type: String, required: true }
	});
	
    var User = mongoose.model('User', UserSchema);
    var Order = mongoose.model('Order', OrderSchema);
    var Product = mongoose.model('Product', ProductSchema);
    var Category = mongoose.model('Category', CategorySchema);
}

module.exports.createModels = createModels;