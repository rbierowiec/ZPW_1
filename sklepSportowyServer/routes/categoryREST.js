function registerServices(app){
	var categoriesManager = require('../controllers/categoryController.js');
	var tokenProvider = require('../controllers/tokenCheck.js');
	
	app.get('/categories', function (req, res) {
		categoriesManager.getAllCategories(app, req, res);
	})
	app.post('/categories', function (req, res) {
		tokenProvider.checkToken(app, req, res, categoriesManager.createNewCategory);
	})
	app.delete('/categories/:id', function (req, res) {
		tokenProvider.checkToken(app, req, res, categoriesManager.deleteCategory);
	})
	app.put('/categories/:id', function (req, res) {
		tokenProvider.checkToken(app, req, res, categoriesManager.updateCategory);
	})
}

module.exports.registerServices = registerServices;