function registerServices(app){
	var ordersManager = require('../controllers/orderController.js');
	var tokenProvider = require('../controllers/tokenCheck.js');

	app.get('/orders', function (req, res) {
		tokenProvider.checkToken(app, req, res, ordersManager.getAllOrders);
	})
	app.get('/orders/me', function (req, res) {
		tokenProvider.checkToken(app, req, res, ordersManager.getAllUserOrders);
	})
	app.post('/orders', function (req, res) {
		tokenProvider.checkToken(app, req, res, ordersManager.createNewOrder, false);
	})
	app.delete('/orders/:id', function (req, res) {
		tokenProvider.checkToken(app, req, res, ordersManager.deleteOrder);
	})
	app.put('/orders/:id', function (req, res) {
		tokenProvider.checkToken(app, req, res, ordersManager.updateOrder);
	})
}

module.exports.registerServices = registerServices;