function registerServices(app){
	var productsManager = require('../controllers/productController.js');
	var tokenProvider = require('../controllers/tokenCheck.js');
	
	app.get('/products', function (req, res) {
		productsManager.getAllProducts(app, req, res);
	})
	app.post('/products', function (req, res) {
		tokenProvider.checkToken(app, req, res, productsManager.createNewProduct);
	})
	app.delete('/products/:id', function (req, res) {
		tokenProvider.checkToken(app, req, res, productsManager.deleteProduct);
	})
	app.put('/products/:id', function (req, res) {
		tokenProvider.checkToken(app, req, res, productsManager.updateProduct);
	})
}

module.exports.registerServices = registerServices;