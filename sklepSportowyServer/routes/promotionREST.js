function registerServices(app){
	var promotionManager = require('../controllers/promotionController.js');
	var tokenProvider = require('../controllers/tokenCheck.js');
	
	app.get('/promotion', function (req, res) {
		tokenProvider.checkToken(app, req, res, promotionManager.getAllPromotions);
		// promotionManager.getAllPromotions(app, req, res);
	})
	app.post('/promotion', function ( req, res) {
		tokenProvider.checkToken(app, req, res, promotionManager.createNewPromotion);
		// promotionManager.createNewPromotion(app, req, res);
	})
	app.delete('/promotion/:id', function (req, res) {
		tokenProvider.checkToken(app, req, res, promotionManager.deletePromotion);
		// promotionManager.deletePromotion(app, req, res, io);
	})
}

module.exports.registerServices = registerServices;