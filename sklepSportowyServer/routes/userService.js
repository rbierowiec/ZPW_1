function registerServices(app){
	var usersManager = require('../controllers/userController.js');
	var tokenProvider = require('../controllers/tokenCheck.js');
	
	app.post('/users/login', function (req, res) {
		usersManager.loginUser(app, req, res);
	})
	app.post('/users/register', function (req, res) {
		usersManager.registerUser(app, req, res);
		// tokenProvider.checkToken(app, req, res, usersManager.registerUser);
	})
}

module.exports.registerServices = registerServices;