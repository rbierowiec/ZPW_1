var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', { useMongoClient: true })
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'błąd połączenia...'));
db.once('open', function() {
	console.log("połączenie udane!");
});

var bodyParser  = require('body-parser');
var express 	= require('express');
var app 		= express();

	
app.use(bodyParser({limit: '50mb'})); 
app.use(function(req, res, next) {
	var allowedOrigins = ['http://localhost:4200','http://127.0.0.1:4200','http://localhost:4201','http://127.0.0.1:4201'];
	var origin = req.headers.origin;
	if(allowedOrigins.indexOf(origin) > -1){
		 res.setHeader('Access-Control-Allow-Origin', origin);
	}
	res.header("Access-Control-Allow-Credentials", true);
	res.header("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-Access-Token");
	next();
});

require('./models/db.js').createModels(mongoose.Schema, mongoose);
require('./routes/userService.js').registerServices(app);
require('./routes/orderREST.js').registerServices(app);
require('./routes/productREST.js').registerServices(app);
require('./routes/categoryREST.js').registerServices(app);


var port = process.env.PORT || '5500';
app.set('port',port);
var server = require('http' ).createServer(app);


var io = require('socket.io')(server)
app.set('io_socket', io);
// console.log(app.get('io_socket'));
var promotionManager = require('./controllers/promotionController.js');
promotionManager.createSocketInstance(app);
require('./routes/promotionREST.js').registerServices(app);
// var promotionTime = 1;

// promotionManager.generatePromotion(io, 50, promotionTime);
// setInterval(function(){
// 	promotionManager.generatePromotion(io, 50, promotionTime);
// }, promotionTime * 60 * 1000);

server.listen(5500);